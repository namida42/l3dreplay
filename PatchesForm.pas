unit PatchesForm;

{$mode objfpc}{$H+}

interface

uses
  Misc,
  MusicPatchSelectTracksForm,
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TFL3DPatches }

  TFL3DPatches = class(TForm)
    btnExit: TButton;
    btnMusicPatch: TButton;
    btnPyramidPatch: TButton;
    btnCDToFloppy: TButton;
    lblInfo: TLabel;
    procedure btnCDToFloppyClick(Sender: TObject);
    procedure btnCDToFloppyMouseDown(Sender: TObject; Button: TMouseButton;
      {%H-}Shift: TShiftState; {%H-}X, {%H-}Y: Integer);
    procedure btnMusicPatchClick(Sender: TObject);
    procedure btnMusicPatchMouseDown(Sender: TObject; Button: TMouseButton;
      {%H-}Shift: TShiftState; {%H-}X, {%H-}Y: Integer);
    procedure btnPyramidPatchClick(Sender: TObject);
    procedure btnPyramidPatchMouseDown(Sender: TObject; Button: TMouseButton;
      {%H-}Shift: TShiftState; {%H-}X, {%H-}Y: Integer);
  private

  public

  end;

implementation

{$R *.lfm}

{ TFL3DPatches }

procedure TFL3DPatches.btnCDToFloppyClick(Sender: TObject);
var
  RS: TResourceStream;
  MS: TMemoryStream;

  SignatureOffset: Cardinal;
  SignatureCount: Byte;
  CheckByte, ExpectedByte: Byte;

  PatchOffset: Cardinal;
  PatchCount: Byte;
  PatchByte: Byte;
  PatchPointer: ^Byte;

  i: Integer;

  MsgText: String;
const
  VERSION_CHECK_OFFSET = $000243F1;
  FLOPPY_MASK = $04; // this bit is set on the CD version but not the floppy version
begin
  RS := TResourceStream.Create(HInstance, 'PATCH', 'CDFLOPPY');
  MS := TMemoryStream.Create;
  try
    MS.LoadFromFile(AppPath + 'L3D.EXE');

    RS.Read(SignatureOffset{%H-}, 4);
    RS.Read(SignatureCount{%H-}, 1);

    MS.Position := SignatureOffset;

    for i := 0 to SignatureCount-1 do
    begin
      RS.Read(ExpectedByte{%H-}, 1);
      MS.Read(CheckByte{%H-}, 1);
      if ExpectedByte <> CheckByte then
      begin
        ShowMessage('Patch could not be applied - target file appears to be invalid.');
        Exit;
      end;
    end;

    MS.Position := VERSION_CHECK_OFFSET;
    MS.Read(CheckByte, 1);
    if (CheckByte and FLOPPY_MASK) = 0 then
      MsgText := 'This will convert the floppy disk version of L3D to the CD version. Continue?'
    else
      MsgText := 'This will convert the CD version of L3D to the floppy disk version. Continue?';

    if not MessageDlg('Patch', MsgText, mtCustom, [mbYes, mbNo], 0) = mrYes then
      Exit;

    while RS.Position < RS.Size do
    begin
      RS.Read(PatchOffset{%H-}, 4);
      RS.Read(PatchCount{%H-}, 1);

      PatchPointer := MS.Memory + PatchOffset;
      for i := 0 to PatchCount-1 do
      begin
        RS.Read(PatchByte{%H-}, 1);
        PatchPointer^ := PatchPointer^ xor PatchByte;
        Inc(PatchPointer);
      end;
    end;

    MS.SaveToFile(AppPath + 'L3D.EXE');
  finally
    MS.Free;
    RS.Free;
  end;
end;

procedure TFL3DPatches.btnCDToFloppyMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    ShowMessage('This will turn the Lemmings 3D CD version''s EXE into that of the floppy disk version, or vice versa.');
end;

procedure TFL3DPatches.btnMusicPatchClick(Sender: TObject);
var
  RS: TResourceStream;
  SL: TStringList;
  n: Integer;

  F: TFMusicPatchSelectTracks;
begin
  if FileExists(AppPath + 'SOUND\patched') then
  begin
    ShowMessage('It appears the music patch has already been applied to this copy of Lemmings 3D.');
    Exit;
  end;

  SL := TStringList.Create;
  try
    RS := TResourceStream.Create(HInstance, 'MISC', 'MUSICSWAPS');
    try
      SL.LoadFromStream(RS);
    finally
      RS.Free;
    end;

    n := 0;
    while n < SL.Count do
    begin
      RenameFile(AppPath + SL[n], AppPath + SL[n+1] + '.tmp');
      RenameFile(AppPath + SL[n+1], AppPath + SL[n]);
      RenameFile(AppPath + SL[n+1] + '.tmp', SL[n+1]);
      Inc(n, 2);
    end;

    SL.Clear;
    SL.SaveToFile(AppPath + 'SOUND\patched');
  finally
    SL.Free;
  end;

  F := TFMusicPatchSelectTracks.Create(self);
  try
    F.ShowModal;
  finally
    F.Free;
  end;

  ShowMessage('Music patch complete!');
end;

procedure TFL3DPatches.btnMusicPatchMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    ShowMessage('This patch fixes the discrepancy between CD audio music selection and soundcard / MIDI music selection. You will be offered the choice of which version''s selection to treat as the correct one.');
end;

procedure TFL3DPatches.btnPyramidPatchClick(Sender: TObject);
var
  Tex1, Tex2: TMemoryStream;
  Tex0: TMemoryStream;
  Patch: TMemoryStream;

  RS: TResourceStream;

  T0Ptr, T1Ptr, T2Ptr, PatchPtr: ^Byte;
  i: Integer;
begin
  Tex0 := TMemoryStream.Create;
  Tex1 := TMemoryStream.Create;
  Tex2 := TMemoryStream.Create;
  Patch := TMemoryStream.Create;
  try
    RS := TResourceStream.Create(HInstance, 'PATCH', 'PYRAMID');
    try
      Patch.LoadFromStream(RS);
    finally
      RS.Free;
    end;

    try
      Tex1.LoadFromFile(AppPath + 'GFX/TEXTURE.001');
      Tex2.LoadFromFile(AppPath + 'GFX/TEXTURE.002');
    except
      ShowMessage('Lemmings 3D''s graphic files were not found.');
      Exit;
    end;
    Tex0.SetSize(Tex1.Size);

    T0Ptr := Tex0.Memory;
    T1Ptr := Tex1.Memory;
    T2Ptr := Tex2.Memory;
    PatchPtr := Patch.Memory;

    for i := 0 to Tex0.Size-1 do
    begin
      T0Ptr^ := T1Ptr^ xor T2Ptr^ xor PatchPtr^;
      Inc(T0Ptr);
      Inc(T1Ptr);
      Inc(T2Ptr);
      Inc(PatchPtr);
    end;

    Tex0.SaveToFile(AppPath + 'GFX/TEXTURE.000');
    ShowMessage('Pyramid texture patch successfully applied.');
  finally
    Tex0.Free;
    Tex1.Free;
    Tex2.Free;
    Patch.Free;
  end;
end;

procedure TFL3DPatches.btnPyramidPatchMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    ShowMessage('This fixes the graphical glitches some copies of Lemmings 3D have on certain pyramid levels.');
end;

end.


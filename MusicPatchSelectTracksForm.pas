unit MusicPatchSelectTracksForm;

// Copyright (c) 2019 Namida Verasche
// Made available under the MIT Licence. See LICENCE.TXT for more info.

interface

uses
  Misc, L3DDecompressor,
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls;

type

  { TFMusicPatchSelectTracks }

  TFMusicPatchSelectTracks = class(TForm)
    btnAllCD: TButton;
    btnAllMidi: TButton;
    btnOk: TButton;
    gbTrackChoices: TGroupBox;
    lblSetAll: TLabel;
    lblWhichVersion: TLabel;
    lblPleaseSelect: TLabel;
    rgCastle: TRadioGroup;
    rbCustomLevels: TRadioButton;
    rbOfficialLevels: TRadioButton;
    rgPyramid: TRadioGroup;
    rgSpace: TRadioGroup;
    rgCandy: TRadioGroup;
    rgGolf: TRadioGroup;
    rgComputer: TRadioGroup;
    rgArmy: TRadioGroup;
    rgMaze: TRadioGroup;
    rgCircus: TRadioGroup;
    rgLemgo: TRadioGroup;
    procedure btnAllCDClick(Sender: TObject);
    procedure btnAllMidiClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure MusicOptionClick(Sender: TObject);
    procedure rbCustomLevelsChange(Sender: TObject);
    procedure rbOfficialLevelsChange(Sender: TObject);
  private
    fSwapMusic: array[0..10] of Boolean;
    procedure ApplyLevelPatches;
  public

  end;

implementation

{$R *.lfm}

{ TFMusicPatchSelectTracks }

procedure TFMusicPatchSelectTracks.FormCreate(Sender: TObject);
begin
  gbTrackChoices.BorderWidth := 0;
  fSwapMusic[3] := true; // Swap space, but nothing else, by default. It fits so much better when swapped.
end;

procedure TFMusicPatchSelectTracks.btnAllCDClick(Sender: TObject);
begin
  rgCastle.ItemIndex := 0;
  rgPyramid.ItemIndex := 0;
  rgSpace.ItemIndex := 0;
  rgCandy.ItemIndex := 0;
  rgGolf.ItemIndex := 0;
  rgComputer.ItemIndex := 0;
  rgArmy.ItemIndex := 0;
  rgMaze.ItemIndex := 0;
  rgCircus.ItemIndex := 0;
  rgLemgo.ItemIndex := 0;
end;

procedure TFMusicPatchSelectTracks.btnAllMidiClick(Sender: TObject);
begin
  rgCastle.ItemIndex := 1;
  rgPyramid.ItemIndex := 1;
  rgSpace.ItemIndex := 1;
  rgCandy.ItemIndex := 1;
  rgGolf.ItemIndex := 1;
  rgComputer.ItemIndex := 1;
  rgArmy.ItemIndex := 1;
  rgMaze.ItemIndex := 1;
  rgCircus.ItemIndex := 1;
  rgLemgo.ItemIndex := 1;
end;

procedure TFMusicPatchSelectTracks.btnOkClick(Sender: TObject);
begin
  if rbOfficialLevels.Checked then ApplyLevelPatches;
  ModalResult := mrOk;
end;

procedure TFMusicPatchSelectTracks.MusicOptionClick(Sender: TObject);
begin
  if not (Sender is TRadioGroup) then Exit;
  fSwapMusic[TRadioGroup(Sender).Tag] := TRadioGroup(Sender).ItemIndex = 1;
end;

procedure TFMusicPatchSelectTracks.rbCustomLevelsChange(Sender: TObject);
begin
  gbTrackChoices.Enabled := false;
end;

procedure TFMusicPatchSelectTracks.rbOfficialLevelsChange(Sender: TObject);
begin
  gbTrackChoices.Enabled := true;
end;

procedure TFMusicPatchSelectTracks.ApplyLevelPatches;
var
  b: Byte;
  i: Integer;
  F: TFileStream;

  SignatureCheck: array[0..2] of AnsiChar;
  EncounteredCompressed: Boolean;
begin
  b := 0;
  EncounteredCompressed := false;

  for i := 0 to 11 do
    if i = 11 then
      Exit
    else if fSwapMusic[i] then
      Break; // Save time if we aren't applying any swaps.

  for i := 0 to 99 do
  begin
    F := TFileStream.Create(AppPath + 'LEVELS\LEVEL.' + LeadZeroStr(i, 3), fmOpenReadWrite);
    try
      F.Position := 0;
      F.Read({%H-}SignatureCheck[0], 3);
      if SignatureCheck = 'RNC' then
      begin
        F.Free;

        if not EncounteredCompressed then
        begin
          ShowMessage('Applying the patch to levels may take some time due to the need to decompress the levels. During this time, L3DUtils ' +
                      'will be unresponsive. Click OK to continue.');
          EncounteredCompressed := true;
        end;

        F := nil;
        DecompressFile(AppPath + 'LEVELS\LEVEL.' + LeadZeroStr(i, 3));
        F := TFileStream.Create(AppPath + 'LEVELS\LEVEL.' + LeadZeroStr(i, 3), fmOpenReadWrite);
      end;

      F.Position := $0143;
      F.Read(b, 1);
      if (b > 10 {just in case}) or not fSwapMusic[b] then Continue;

      F.Position := $0151;
      F.Read(b, 1);

      case b of
        0: Continue;
        1: b := 2;
        2: b := 1;
      end;

      F.Position := $0151;
      F.Write(b, 1);
    finally
      F.Free;
    end;
  end;
end;

end.


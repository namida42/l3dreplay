program L3DUtils;

// Copyright 2019 Namida Verasche
// L3DReplay's source code is made available under the MIT Licence.
// See Licence.txt for more information.

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, L3DReplay, L3DUtilsMain, Settings, PatchesForm, MusicPatchSelectTracksForm
  { you can add units after this };

{$R *.res}
{$R utils/resources.rc}

begin
  RequireDerivedFormResource:=True;
  Application.Scaled:=True;
  Application.Initialize;
  Application.CreateForm(TFL3DUtilsMain, FL3DUtilsMain);
  Application.Run;
end.


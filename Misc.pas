unit Misc;

// Copyright (c) 2019 Namida Verasche
// Made available under the MIT Licence. See LICENCE.TXT for more info.

{$MODESWITCH ADVANCEDRECORDS}

interface

uses
  Forms,
  Classes, SysUtils;

const
  VERSION_STRING = '0.10';

  function AppPath: String;
  function LeadZeroStr(aValue: Integer; aLength: Integer; aLengthIncludesNegativeSign: Boolean = false): String;

implementation

var
  _AppPath: String;

function AppPath: String;
begin
  if _AppPath = '' then
    _AppPath := IncludeTrailingPathDelimiter(ExtractFilePath(Application.ExeName));

  Result := _AppPath;
end;

function LeadZeroStr(aValue: Integer; aLength: Integer; aLengthIncludesNegativeSign: Boolean = false): String;
var
  IsNegative: Boolean;
  PadCount: Integer;
begin
  IsNegative := aValue < 0;
  aValue := Abs(aValue);

  Result := IntToStr(aValue);

  PadCount := aLength - Length(Result);
  if IsNegative and aLengthIncludesNegativeSign then
    PadCount := PadCount - 1;

  if PadCount > 0 then
    Result := StringOfChar('0', PadCount) + Result;

  if IsNegative then
    Result := '-' + Result;
end;

end.


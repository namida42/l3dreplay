unit Settings;

{$mode objfpc}{$H+}

interface

uses
  Misc,
  Classes, SysUtils;

type

  { TSettings }

  TSettings = class
    public
      ReplayAutoSaveName: String;
      LastKnownBase: Pointer;

      DosboxPath: String;
      IsoPath: String;
      OpticalDrive: String;
      DiscOption: Integer;
      SaveName: String;

      procedure Load;
      procedure Save;
  end;

var
  Options: TSettings;

implementation

{ TSettings }

procedure TSettings.Load;
var
  SL: TStringList;
begin
  SL := TStringList.Create;
  try
    if FileExists(AppPath + 'L3DUtils.ini') then
      SL.LoadFromFile(AppPath + 'L3DUtils.ini'); // We don't exit, so that defaults still get applied.

    LastKnownBase := Pointer(StrToIntDef('0x' + SL.Values['LAST_KNOWN_OFFSET'], 0));
    ReplayAutoSaveName := SL.Values['AUTOSAVE'];

    DiscOption := StrToIntDef(SL.Values['DISC_TYPE'], 2);
    DosboxPath := SL.Values['DOSBOX'];
    IsoPath := SL.Values['ISO'];
    OpticalDrive := SL.Values['DRIVE'];

    SaveName := SL.Values['SAVE'];
  finally
    SL.Free;
  end;
end;

procedure TSettings.Save;
var
  SL: TStringList;
begin
  SL := TStringList.Create;
  try
    if {%H-}Integer(LastKnownBase) <> 0 then
      SL.Values['LAST_KNOWN_OFFSET'] := IntToHex(Integer(LastKnownBase), 8);

    if ReplayAutoSaveName <> '' then
      SL.Values['AUTOSAVE'] := ReplayAutoSaveName;

    SL.Values['DISC_TYPE'] := IntToStr(DiscOption);
    SL.Values['DOSBOX'] := DosboxPath;
    case DiscOption of
      0: SL.Values['ISO'] := IsoPath;
      1: SL.Values['DRIVE'] := OpticalDrive;
    end;

    SL.Values['SAVE'] := SaveName;

    SL.SaveToFile(AppPath + 'L3DUtils.ini');
  finally
    SL.Free;
  end;
end;

end.


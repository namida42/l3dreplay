unit L3DDecompressor;

// Copyright (c) 2019 Namida Verasche
// Made available under the MIT Licence. See LICENCE.TXT for more info.

interface

uses
  Misc,
  Classes, SysUtils, Process, LazFileUtils;

  procedure CompressFile(Filename: String);
  procedure DecompressFile(Filename: String);
  procedure CompressFiles(Fileset: String; BasePath: String = '');
  procedure DecompressFiles(Fileset: String; BasePath: String = '');

const
  COMPRESSION_RESOURCE = 'CMPINFO';

  FILE_SET_L3D_FULL = 'L3D';
  FILE_SET_L3D_WINTERLAND = 'L3DW';
  FILE_SET_L3D_DEMO_1 = 'L3DD1';
  FILE_SET_L3D_DEMO_2 = 'L3DD2';

  EXTRACTOR_FILE = 'rnc.exe';
  EXTRACTOR_RESOURCE = 'RNCTEST';

implementation

procedure PrepareExtractor;
var
  ResourceStream: TResourceStream;
  FileStream: TFileStream;
begin
  ResourceStream := TResourceStream.Create(HInstance, COMPRESSION_RESOURCE, EXTRACTOR_RESOURCE);
  try
    FileStream := TFileStream.Create(AppPath + EXTRACTOR_FILE, fmCreate);
    try
      FileStream.CopyFrom(ResourceStream, ResourceStream.Size);
    finally
      FileStream.Free;
    end;
  finally
    ResourceStream.Free;
  end;
end;

procedure RemoveExtractor;
begin
  if FileExists(AppPath + EXTRACTOR_FILE) then
    DeleteFile(AppPath + EXTRACTOR_FILE);
end;

procedure LoadResourceStringlist(Fileset: String; StringList: TStringList);
var
  ResourceStream: TResourceStream;
begin
  ResourceStream := TResourceStream.Create(HInstance, COMPRESSION_RESOURCE, PChar(Fileset));
  try
    StringList.LoadFromStream(ResourceStream);
  finally
    ResourceStream.Free;
  end;
end;

procedure InternalCompressFile(Filename: String);
var
  params: array of String;
  Dummy: String;
begin
  SetLength(params, 4);
  params[0] := 'p';
  params[1] := Filename;
  params[2] := Filename + '.tmp';
  params[3] := '-m=1';
  RunCommand(AppPath + EXTRACTOR_FILE, params, Dummy, [TProcessOption.poNoConsole]);
  FileSetAttr(Filename, FileGetAttr(Filename) and not faReadOnly);
  DeleteFile(Filename);
  RenameFile(Filename + '.tmp', Filename);
end;

procedure InternalDecompressFile(Filename: String);
var
  params: array of String;
  Dummy: String;
begin
  SetLength(params, 3);
  params[0] := 'u';
  params[1] := Filename;
  params[2] := Filename + '.tmp';
  RunCommand(AppPath + EXTRACTOR_FILE, params, Dummy, [TProcessOption.poNoConsole]);
  FileSetAttr(Filename, FileGetAttr(Filename) and not faReadOnly);
  DeleteFile(Filename);
  RenameFile(Filename + '.tmp', Filename);
end;

procedure CompressFiles(Fileset: String; BasePath: String = '');
var
  SL: TStringList;
  S: String;
begin
  if (BasePath = '') then BasePath := AppPath;

  PrepareExtractor;
  SL := TStringList.Create;
  try
    LoadResourceStringList(Fileset, SL);

    for S in SL do
      InternalCompressFile(S);
  finally
    SL.Free;
    RemoveExtractor;
  end;
end;

procedure DecompressFiles(Fileset: String; BasePath: String = '');
var
  SL: TStringList;
  S: String;
begin
  if (BasePath = '') then BasePath := AppPath;

  PrepareExtractor;
  SL := TStringList.Create;
  try
    LoadResourceStringList(Fileset, SL);

    for S in SL do
      InternalDecompressFile(S);
  finally
    SL.Free;
    RemoveExtractor;
  end;
end;

procedure CompressFile(Filename: String);
begin
  PrepareExtractor;
  try
    InternalCompressFile(Filename);
  finally
    RemoveExtractor;
  end;
end;

procedure DecompressFile(Filename: String);
begin
  PrepareExtractor;
  try
    InternalDecompressFile(Filename);
  finally
    RemoveExtractor;
  end;
end;

end.


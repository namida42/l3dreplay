unit L3DUtilsMain;

{$mode objfpc}{$H+}

interface

uses
  L3DDecompressor,
  PatchesForm,
  L3DReplay, Settings, Misc,
  FileUtil,
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ShellApi;

type

  { TFL3DUtilsMain }

  TFL3DUtilsMain = class(TForm)
    btnISOLocate: TButton;
    btnReplayManager: TButton;
    btnExit: TButton;
    btnPatches: TButton;
    btnDosboxLocate: TButton;
    btnRun: TButton;
    btnSetup: TButton;
    btnUnlockAllLevels: TButton;
    cbSaveFile: TComboBox;
    ebDosboxPath: TEdit;
    ebISOPath: TEdit;
    ebDriveLetter: TEdit;
    gbLauncher: TGroupBox;
    lblSaveFile: TLabel;
    lblDosbox: TLabel;
    rbNoDisc: TRadioButton;
    rbISO: TRadioButton;
    rbDrive: TRadioButton;
    procedure btnDosboxLocateClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnISOLocateClick(Sender: TObject);
    procedure btnReplayManagerClick(Sender: TObject);
    procedure btnPatchesClick(Sender: TObject);
    procedure btnRunClick(Sender: TObject);
    procedure btnSetupClick(Sender: TObject);
    procedure btnUnlockAllLevelsClick(Sender: TObject);
    procedure cbSaveFileChange(Sender: TObject);
    procedure ebDosboxPathChange(Sender: TObject);
    procedure ebDriveLetterChange(Sender: TObject);
    procedure ebISOPathChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure rbDriveChange(Sender: TObject);
    procedure rbISOChange(Sender: TObject);
    procedure rbNoDiscChange(Sender: TObject);
  private
    fReplayForm: TFL3DReplayForm;
    fLastSaveFile: String;

    procedure RunDosbox(ExeName: String);
  public

  end;

var
  FL3DUtilsMain: TFL3DUtilsMain;

implementation

{$R *.lfm}

{ TFL3DUtilsMain }

procedure TFL3DUtilsMain.btnExitClick(Sender: TObject);
begin
  Close;
end;

procedure TFL3DUtilsMain.btnISOLocateClick(Sender: TObject);
var
  OpenDlg: TOpenDialog;
begin
  OpenDlg := TOpenDialog.Create(self);
  try
    OpenDlg.Title := 'Select Lemmings 3D ISO';
    OpenDlg.Filter := 'Disc images|*.iso;*.cue';
    OpenDlg.Options := [ofHideReadOnly, ofFileMustExist];
    if OpenDlg.Execute then
      ebIsoPath.Text := OpenDlg.Filename;
  finally
    OpenDlg.Free;
  end;
end;

procedure TFL3DUtilsMain.btnDosboxLocateClick(Sender: TObject);
var
  OpenDlg: TOpenDialog;
begin
  OpenDlg := TOpenDialog.Create(self);
  try
    OpenDlg.Title := 'Select DOSBox EXE';
    OpenDlg.Filter := 'Executable files|*.exe';
    OpenDlg.Options := [ofHideReadOnly, ofFileMustExist];
    if OpenDlg.Execute then
      ebDosboxPath.Text := OpenDlg.Filename;
  finally
    OpenDlg.Free;
  end;
end;

procedure TFL3DUtilsMain.btnReplayManagerClick(Sender: TObject);
begin
  if fReplayForm = nil then
    fReplayForm := TFL3DReplayForm.Create(self);

  fReplayForm.Show;
  fReplayForm.BringToFront;
end;

procedure TFL3DUtilsMain.btnPatchesClick(Sender: TObject);
var
  PatchForm: TFL3DPatches;
begin
  PatchForm := TFL3DPatches.Create(self);
  try
    PatchForm.ShowModal;
  finally
    PatchForm.Free;
  end;
end;

procedure TFL3DUtilsMain.btnRunClick(Sender: TObject);
begin
  RunDosbox('L3D.EXE');
end;

procedure TFL3DUtilsMain.btnSetupClick(Sender: TObject);
begin
  RunDosbox('SETUP.EXE');
end;

procedure TFL3DUtilsMain.btnUnlockAllLevelsClick(Sender: TObject);
var
  SaveFilePath: String;

  SaveFile: TMemoryStream;
  LevelFile: TFileStream;
  i: Integer;

  Title: packed array[0..31] of AnsiChar;
  Completion: Byte;
  RNCHeader: packed array[0..2] of AnsiChar;

  Checksum: Word;
  RemainingCount: Word;
  WordPtr: ^Word;

  CompressionWarning: Boolean;
begin
  CompressionWarning := false;

  if Options.SaveName = '' then
    cbSaveFile.Text := 'DEFAULT';

  SaveFilePath := AppPath + 'SAVE\' + Options.SaveName + '\LM3D.CD\LM3D.SAV';

  SaveFile := TMemoryStream.Create;
  try
    if FileExists(SaveFilePath) then
    begin
      SaveFile.LoadFromFile(SaveFilePath);
      SaveFile.Size := 3719;
    end else begin
      SaveFile.Size := 3719;
      FillChar(SaveFile.Memory^, 3719, 0);
    end;

    for i := 0 to 79 do
    begin
      LevelFile := TFileStream.Create(AppPath + 'LEVELS\LEVEL.' + LeadZeroStr(i, 3), fmOpenRead);
      try
        LevelFile.Read({%H-}RNCHeader[0], 3);
        if RNCHeader = 'RNC' then
        begin
          if not CompressionWarning then
          begin
            CompressionWarning := true;
            ShowMessage('Unlocking all levels may take some time due to the need to decompress the levels. During this time, L3DUtils ' +
                        'will be unresponsive. This will only be needed the first time you use the Unlock All Levels feature. Click OK to continue.');
          end;

          LevelFile.Free;
          DecompressFile(AppPath + 'LEVELS\LEVEL.' + LeadZeroStr(i, 3));
          LevelFile := TFileStream.Create(AppPath + 'LEVELS\LEVEL.' + LeadZeroStr(i, 3), fmOpenRead);
        end;

        LevelFile.Position := $F0;
        LevelFile.Read({%H-}Title[0], 32);

        SaveFile.Position {%H-}:= (i * $24) + 2;
        SaveFile.Write(Title[0], 32);

        SaveFile.Position {%H-}:= (((i + 1) * $24) + 2) - 1; // yes, I could just do +1, it's just to make it more clear what's going on
        SaveFile.Read(Completion{%H-}, 1);
        if Completion = $00 then // we don't want to overwrite if its $01
        begin
          Completion := $02;
          SaveFile.Position {%H-}:= (((i + 1) * $24) + 2) - 1;
          SaveFile.Write(Completion, 1);
        end;
      finally
        LevelFile.Free;
      end;
    end;

    WordPtr := SaveFile.Memory + 2; // skip the checksum bytes themself
    RemainingCount := 1858;
    Checksum := 0;

    while (RemainingCount > 0) do
    begin
      Checksum := (Checksum + WordPtr^) and $FFFF;
      Checksum := ((Checksum and $FFF8) shr 3) {%H-}or ((Checksum and $0007) shl 13);
      Checksum := Checksum xor RemainingCount;
      Dec(RemainingCount);
      Inc(WordPtr);
    end;

    SaveFile.Position := 0;
    SaveFile.Write(Checksum, 2);

    ForceDirectories(ExtractFilePath(SaveFilePath));
    SaveFile.SaveToFile(SaveFilePath);
  finally
    SaveFile.Free;
  end;
end;

{procedure TFL3DEdit.miCreateSaveFileClick(Sender: TObject);
var
  MS: TMemoryStream;
  SaveDlg: TSaveDialog;

  Level: TL3DLevel;
  i: Integer;

  Title: packed array[0..31] of AnsiChar;

  Checksum: Word;
  RemainingCount: Word;
  WordPtr: ^Word;
begin
  SaveDlg := TSaveDialog.Create(self);
  try
    SaveDlg.Title := 'All levels save';
    SaveDlg.Options := [ofOverwritePrompt];
    SaveDlg.Filter := 'L3D Save File|LM3D.SAV';
    if SaveDlg.Execute then
    begin
      MS := TMemoryStream.Create;
      Level := TL3DLevel.Create;
      try
        Checksum := 0; // using as a placeholder for now
        MS.Write(Checksum, 2);

        for i := 0 to 79 do // practice levels aren't written in this way. Their completion is tracked, but we don't care about that.
        begin
          Level.LoadFromFile(AppPath + 'LEVELS\LEVEL.' + LeadZeroStr(i, 3));
          FillChar(Title, 32, 0);
          Title := Level.Title;

          Checksum := 0;
          MS.Write(Title[0], 32);
          MS.Write(Checksum, 2); // We don't need to write anything meaningful any non-title bytes.
          Checksum := $0200;
          MS.Write(Checksum, 2);
        end;

        while MS.Size < $0E87 do
          MS.Write(Checksum, 1); // Same, but it's an odd number of bytes so we write one at a time.

        WordPtr := MS.Memory + 2; // skip the checksum bytes themself
        RemainingCount := 1858;
        Checksum := 0;

        while (RemainingCount > 0) do
        begin
          Checksum := (Checksum + WordPtr^) and $FFFF;
          Checksum := ((Checksum and $FFF8) shr 3) {%H-}or ((Checksum and $0007) shl 13);
          Checksum := Checksum xor RemainingCount;
          Dec(RemainingCount);
          Inc(WordPtr);
        end;

        MS.Position := 0;
        MS.Write(Checksum, 2);

        MS.SaveToFile(SaveDlg.Filename);
      finally
        MS.Free;
        Level.Free;
      end;
    end;
  finally
    SaveDlg.Free;
  end;
end;}

procedure TFL3DUtilsMain.cbSaveFileChange(Sender: TObject);
begin
  Options.SaveName := cbSaveFile.Text;
end;

procedure TFL3DUtilsMain.ebDosboxPathChange(Sender: TObject);
begin
  Options.DosboxPath := ebDosboxPath.Text;
end;

procedure TFL3DUtilsMain.ebDriveLetterChange(Sender: TObject);
begin
  Options.OpticalDrive := ebDriveLetter.Text;
end;

procedure TFL3DUtilsMain.ebISOPathChange(Sender: TObject);
begin
  Options.IsoPath := ebIsoPath.Text;
end;

procedure TFL3DUtilsMain.FormCreate(Sender: TObject);
var
  SearchRec: TSearchRec;
begin
  cbSaveFile.Sorted := true;

  Options := TSettings.Create;
  Options.Load;

  Caption := 'L3DUtils V' + VERSION_STRING;

  if FileExists(AppPath + 'SAVE\' + Options.SaveName + '\LM3D.CD\LM3D.CFG') then
    CopyFile(AppPath + 'SAVE\' + Options.SaveName + '\LM3D.CD\LM3D.CFG',
             AppPath + 'SAVE\LM3D.CFG');

  ebDosboxPath.Text := Options.DosboxPath;

  cbSaveFile.Text := Options.SaveName;
  fLastSaveFile := Options.SaveName;

  case Options.DiscOption of
    0: begin rbISO.Checked := true; ebISOPath.Text := Options.IsoPath; end;
    1: begin rbDrive.Checked := true; ebDriveLetter.Text := Options.OpticalDrive; end;
    2: rbNoDisc.Checked := true;
  end;

  if FindFirst(AppPath + 'SAVE\*', faDirectory, SearchRec) = 0 then
  begin
    repeat
      if (SearchRec.Name = '..') or (SearchRec.Name = '.') or ((SearchRec.Attr and faDirectory) = 0) then Continue;
      if not DirectoryExists(AppPath + 'SAVE\' + SearchRec.Name + '\LM3D.CD') then Continue;
      cbSaveFile.Items.Add(SearchRec.Name);
    until FindNext(SearchRec) <> 0;
    FindClose(SearchRec);
  end;
end;

procedure TFL3DUtilsMain.FormDestroy(Sender: TObject);
begin
  if fReplayForm <> nil then fReplayForm.Free;

  Options.Save;
  Options.Free;
end;

procedure TFL3DUtilsMain.rbDriveChange(Sender: TObject);
begin
  Options.DiscOption := 1;
  ebIsoPath.Enabled := false;
  btnIsoLocate.Enabled := false;
  ebDriveLetter.Enabled := true;
end;

procedure TFL3DUtilsMain.rbISOChange(Sender: TObject);
begin
  Options.DiscOption := 0;
  ebIsoPath.Enabled := true;
  btnIsoLocate.Enabled := true;
  ebDriveLetter.Enabled := false;
end;

procedure TFL3DUtilsMain.rbNoDiscChange(Sender: TObject);
begin
  Options.DiscOption := 2;
  ebIsoPath.Enabled := false;
  btnIsoLocate.Enabled := false;
  ebDriveLetter.Enabled := false;
end;

procedure TFL3DUtilsMain.RunDosbox(ExeName: String);
var
  SL: TStringList;
  Params: String;
begin
  if not FileExists(Options.DosboxPath) then begin ShowMessage('Please enter the path to your DOSBox executable.'); Exit; end;
  if (Options.DiscOption = 0) and not FileExists(Options.IsoPath) then begin ShowMessage('Please enter the path to your Lemmings 3D ISO file.'); Exit; end;
  if (Options.DiscOption = 1) and not DirectoryExists(Options.OpticalDrive + ':\') then begin ShowMessage('Please enter the letter of your optical drive.'); Exit; end;
  if (Options.SaveName = '') then begin cbSaveFile.Text := 'DEFAULT'; Options.SaveName := 'DEFAULT'; end;

  if cbSaveFile.Items.IndexOf(cbSaveFile.Text) < 0 then
    cbSaveFile.Items.Add(cbSaveFile.Text);

  ForceDirectories(AppPath + 'SAVE\' + Options.SaveName + '\LM3D.CD\');

  if FileExists(AppPath + 'SAVE\' + fLastSaveFile + '\LM3D.CD\LM3D.CFG') then
    CopyFile(AppPath + 'SAVE\' + fLastSaveFile + '\LM3D.CD\LM3D.CFG',
             AppPath + 'SAVE\LM3D.CFG');

  if FileExists(AppPath + 'SAVE\LM3D.CFG') then
    CopyFile(AppPath + 'SAVE\LM3D.CFG',
             AppPath + 'SAVE\' + Options.SaveName + '\LM3D.CD\LM3D.CFG');

  SL := TStringList.Create;
  try
    SL.Delimiter := ' ';
    SL.StrictDelimiter := true;

    SL.Add('-c');
    SL.Add('cycles max');

    SL.Add('-c');
    SL.Add('MOUNT C "' + AppPath + 'SAVE\' + Options.SaveName + '\"');

    case Options.DiscOption of
      0: begin SL.Add('-c'); SL.Add('IMGMOUNT D "' + Options.IsoPath + '" -t iso -fs iso'); end;
      1: begin SL.Add('-c'); SL.Add('MOUNT D ' + Options.OpticalDrive + ':\ -t cdrom'); end;
    end;

    if FileExists(AppPath + ExeName) then
    begin
      SL.Add('-c');
      SL.Add('MOUNT E "' + AppPath + '"');
    end;

    SL.Add('-c');
    if FileExists(AppPath + ExeName) then
      SL.Add('E:')
    else if Options.DiscOption in [0, 1] then
      SL.Add('D:')
    else begin
      ShowMessage('L3DUtils must be in the same directory as L3D to launch a non-CD copy of the game.');
      Exit;
    end;
    SL.Add('-c');
    SL.Add(ExeName);
    SL.Add('-c');
    SL.Add('exit');

    Params := SL.DelimitedText;
    Params := StringReplace(Params, '""', '\"', [rfReplaceAll]);

    ShellExecute(0, nil, PChar(Options.DosboxPath), PChar(Params), PChar(AppPath), 0);
  finally
    SL.Free;
  end;
end;

end.


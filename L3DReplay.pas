unit L3DReplay;

// Copyright 2019 Namida Verasche
// L3DReplay's source code is made available under the MIT Licence.
// See Licence.txt for more information.

{$mode objfpc}{$H+}

interface

uses
  Settings,
  JwaTlHelp32, JwaPSAPI, Windows,
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls;

const
  HEIGHT_HIDE_MEMDUMP = 87;
  MEMDUMP_UNLOCK_CODE = 'PETERGRIFFIN';

// To unlock memory dump features:
//  1. Open replay manager
//  2. Turn on caps lock (holding shift won't work)
//  3. Type the contents of MEMDUMP_UNLOCK_CODE

// To use them - dump is easy enough (it just dumps the entire memory); for inject, enter the offset
// in hexidecimal, with no prefix, then click the button and choose the file with the data to inject.

type

  { TFL3DReplayForm }

  TFL3DReplayForm = class(TForm)
    btnMemoryInject: TButton;
    btnMemoryInjectFromText: TButton;
    btnQuit: TButton;
    btnSaveReplay: TButton;
    btnLoadReplay: TButton;
    btnMemoryDump: TButton;
    cbAutoSave: TCheckBox;
    cbAutoInject: TCheckBox;
    CheckDosboxTimer: TTimer;
    ebInjectOffset: TEdit;
    mmData: TMemo;
    StickyTimer: TTimer;
    procedure btnLoadReplayClick(Sender: TObject);
    procedure btnMemoryDumpClick(Sender: TObject);
    procedure btnMemoryInjectClick(Sender: TObject);
    procedure btnMemoryInjectFromTextClick(Sender: TObject);
    procedure btnQuitClick(Sender: TObject);
    procedure btnSaveReplayClick(Sender: TObject);
    procedure cbAutoInjectChange(Sender: TObject);
    procedure cbAutoSaveChange(Sender: TObject);
    procedure CheckDosboxTimerTimer(Sender: TObject);
    procedure ebInjectOffsetChange(Sender: TObject);
    procedure ebInjectOffsetKeyPress(Sender: TObject; var Key: char);
    procedure FormClose(Sender: TObject; var {%H-}CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var {%H-}Key: Word; Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: char);
    procedure mmDataChange(Sender: TObject);
    procedure mmDataKeyPress(Sender: TObject; var Key: char);
    procedure StickyTimerTimer(Sender: TObject);
  private
    fDosboxPID: DWORD;
    fDosboxHandle: HANDLE;
    fVirtualMemoryBase: Pointer;

    fTimerActionInProgress: Boolean;
    fAutosaveFired: Boolean;

    fUnlockMemdumpProgress: Integer;

    fDataBuffer: array of Byte;

    procedure CheckAutosave;
    procedure ReleaseHandle;
    procedure PerformInitialization(Silent: Boolean = true);
    function PerformValidityCheck(Silent: Boolean = true): Boolean;

    function MakeDataBuffer: Boolean;
  public

  end;

implementation

const
  MEMORY_SIZE = $580000;

  REPLAY_OFFSET_FROM_BASE = $224000;
  REPLAY_SIZE = $FD22;

  STATE_FLAGS_OFFSET = $011642;

  FIRST_ATTEMPT_PATCH_OFFSET = $01E1E4;
  FIRST_ATTEMPT_PATCH_CHECK = $011695;

  LEMMINGS_SAVED_OFFSET = $016B34;
  SAVE_REQUIREMENT_OFFSET = $026666;
  TITLE_OFFSET = $026670;

  SIGNATURE_OFFSET = $1938;
  SIGNATURE_TERMINATED_VALUE = $FFFFFFFF;

{$R *.lfm}

function FindProcess(ExeName: String): DWORD;
var
  Name : String;
  proc : PROCESSENTRY32;
  hSnap : HWND;
  Looper : BOOL;
begin
  Result := $FFFFFFFF;
  proc.dwSize := SizeOf(Proc);
  hSnap := CreateToolhelp32Snapshot(TH32CS_SNAPALL,0);
  try
    Looper := Process32First(hSnap,proc);
    while Integer(Looper) <> 0 do
    begin
      Name := ExtractFileName(proc.szExeFile);
      If ExeName = Name then
      begin
        Result := proc.th32ProcessID;
        Exit;
      end;
      Looper := Process32Next(hSnap,proc);
    end;
  finally
    CloseHandle(hSnap);
  end
end;

{ TFL3DReplayForm }

procedure TFL3DReplayForm.btnLoadReplayClick(Sender: TObject);
var
  Buffer: array[0..REPLAY_SIZE-1] of Byte;
  LevelNumBuffer: array[0..2] of AnsiChar;
  LevelNum: Word;
  StateFlags: Byte;
  FS: TFileStream;
  OpenDlg: TOpenDialog;
begin
  if not PerformValidityCheck(false) then Exit;

  ReadProcessMemory(fDosboxHandle, fVirtualMemoryBase + STATE_FLAGS_OFFSET, @StateFlags, 1, nil);

  OpenDlg := TOpenDialog.Create(self);
  try
    OpenDlg.Title := 'Select a replay file';
    OpenDlg.Filter := 'L3DReplay File|*.L3DR';
    OpenDlg.Options := [ofFileMustExist, ofHideReadOnly];
    if OpenDlg.Execute then
    begin
      FS := TFileStream.Create(OpenDlg.Filename, fmOpenRead);
      try
        FS.Read({%H-}Buffer[0], REPLAY_SIZE);
      finally
        FS.Free;
      end;

      if (StateFlags and $03) = $02 then
      begin
        StateFlags := $0A;
        WriteProcessMemory(fDosboxHandle, fVirtualMemoryBase + STATE_FLAGS_OFFSET, @StateFlags, 1, nil);
      end;

      WriteProcessMemory(fDosboxHandle, fVirtualMemoryBase + REPLAY_OFFSET_FROM_BASE, @Buffer[0], REPLAY_SIZE, nil);

      if (StateFlags and $01) = 0 then
      begin
        if (StateFlags and $02) <> 0 then
        begin
          LevelNum := $FFFF; // lol, variable re-use
          WriteProcessMemory(fDosboxHandle, fVirtualMemoryBase + LEMMINGS_SAVED_OFFSET, @LevelNum, 2, nil);

          StateFlags := $8B;
          WriteProcessMemory(fDosboxHandle, fVirtualMemoryBase + STATE_FLAGS_OFFSET, @StateFlags, 1, nil);
        end else begin
          ReadProcessMemory(fDosboxHandle, fVirtualMemoryBase + FIRST_ATTEMPT_PATCH_CHECK, @LevelNumBuffer[0], 3, nil);
          LevelNum := StrToIntDef(LevelNumBuffer, $FFFF);
          WriteProcessMemory(fDosboxHandle, fVirtualMemoryBase + FIRST_ATTEMPT_PATCH_OFFSET, @LevelNum, 2, nil);
        end;
      end;
    end;
  finally
    OpenDlg.Free;
  end;
end;

procedure TFL3DReplayForm.btnMemoryDumpClick(Sender: TObject);
var
  Buffer: array[0..MEMORY_SIZE-1] of Byte;
  FS: TFileStream;
  SaveDlg: TSaveDialog;
begin
  if not PerformValidityCheck(false) then Exit;

  if ReadProcessMemory(fDosboxHandle, fVirtualMemoryBase, @Buffer[0], MEMORY_SIZE, nil) then
  begin
    SaveDlg := TSaveDialog.Create(self);
    try
      SaveDlg.Title := 'Select file to save memory dump to';
      SaveDlg.Filter := 'Memory dump|*.bin';
      SaveDlg.DefaultExt := '.bin';
      SaveDlg.Options := [ofOverwritePrompt];

      if SaveDlg.Execute then
      begin
        FS := TFileStream.Create(SaveDlg.Filename, fmCreate);
        try
          FS.Write(Buffer[0], MEMORY_SIZE);
        finally
          FS.Free;
        end;
      end;
    finally
      SaveDlg.Free;
    end;
  end else
    ShowMessage('Error extracting from DOSBox memory.');
end;

procedure TFL3DReplayForm.btnMemoryInjectClick(Sender: TObject);
var
  MS: TMemoryStream;
  OpenDlg: TOpenDialog;
begin
  if not PerformValidityCheck(false) then Exit;
  cbAutoInject.Checked := false;
  OpenDlg := TOpenDialog.Create(self);
  try
    OpenDlg.Title := 'Select a binary file';
    OpenDlg.Filter := 'Memory data|*.bin';
    OpenDlg.Options := [ofFileMustExist, ofHideReadOnly];
    if OpenDlg.Execute then
    begin
      MS := TMemoryStream.Create;
      try
        MS.LoadFromFile(OpenDlg.Filename);
        WriteProcessMemory(fDosboxHandle, fVirtualMemoryBase + StrToIntDef('0x' + ebInjectOffset.Text, 0), MS.Memory, MS.Size, nil);
      finally
        MS.Free;
      end;
    end;
  finally
    OpenDlg.Free;
  end;
end;

procedure TFL3DReplayForm.btnMemoryInjectFromTextClick(Sender: TObject);
begin
  cbAutoInject.Checked := false;
  MakeDataBuffer;
  WriteProcessMemory(fDosboxHandle, fVirtualMemoryBase + StrToIntDef('0x' + ebInjectOffset.Text, 0), @fDataBuffer[0], Length(fDataBuffer), nil);
end;

procedure TFL3DReplayForm.btnQuitClick(Sender: TObject);
begin
  Close;
end;

procedure TFL3DReplayForm.btnSaveReplayClick(Sender: TObject);
var
  Buffer: array[0..REPLAY_SIZE-1] of Byte;
  FS: TFileStream;
  SaveDlg: TSaveDialog;
begin
  if not PerformValidityCheck(false) then Exit;

  if ReadProcessMemory(fDosboxHandle, fVirtualMemoryBase + REPLAY_OFFSET_FROM_BASE, @Buffer[0], REPLAY_SIZE, nil) then
  begin
    SaveDlg := TSaveDialog.Create(self);
    try
      SaveDlg.Title := 'Select file to save replay to';
      SaveDlg.Filter := 'L3DReplay File|*.L3DR';
      SaveDlg.DefaultExt := '.l3dr';
      SaveDlg.Options := [ofOverwritePrompt];

      if SaveDlg.Execute then
      begin
        FS := TFileStream.Create(SaveDlg.Filename, fmCreate);
        try
          FS.Write(Buffer[0], REPLAY_SIZE);
        finally
          FS.Free;
        end;
      end;
    finally
      SaveDlg.Free;
    end;
  end else
    ShowMessage('Error extracting replay from DOSBox memory.');
end;

procedure TFL3DReplayForm.cbAutoInjectChange(Sender: TObject);
begin
  if cbAutoInject.Checked then
  begin
    if MakeDataBuffer then
      StickyTimer.Enabled := true
    else
      cbAutoInject.Checked := false;
  end else
    StickyTimer.Enabled := false;
end;

procedure TFL3DReplayForm.cbAutoSaveChange(Sender: TObject);
var
  SaveDlg: TSaveDialog;
begin
  if cbAutoSave.Checked and (Options.ReplayAutoSaveName = '') then
  begin
    SaveDlg := TSaveDialog.Create(self);
    try
      SaveDlg.Title := 'Select file to save replay to';
      SaveDlg.Filter := 'L3DReplay File|*.L3DR';
      SaveDlg.DefaultExt := '.l3dr';
      SaveDlg.Filename := '#TITLE#__#TIMESTAMP#.l3dr';

      if SaveDlg.Execute then
        Options.ReplayAutoSaveName := SaveDlg.Filename
      else
        cbAutoSave.Checked := false;
    finally
      SaveDlg.Free;
    end;
  end else
    Options.ReplayAutoSaveName := '';
end;

procedure TFL3DReplayForm.CheckDosboxTimerTimer(Sender: TObject);
begin
  if not fTimerActionInProgress then
  begin
    fTimerActionInProgress := true;
    try
      if fDosboxHandle = 0 then
        PerformInitialization
      else
        if PerformValidityCheck then
          CheckAutosave;
    finally
      fTimerActionInProgress := false;
    end;
  end;
end;

procedure TFL3DReplayForm.ebInjectOffsetChange(Sender: TObject);
begin
  cbAutoInject.Checked := false;
end;

procedure TFL3DReplayForm.ebInjectOffsetKeyPress(Sender: TObject; var Key: char);
begin
  if (Key >= ' ') and not (Key in ['0'..'9', 'a'..'f', 'A'..'F']) then
    Key := #0;
end;

procedure TFL3DReplayForm.FormClose(Sender: TObject;
  var CloseAction: TCloseAction);
begin
  ReleaseHandle;
  CloseAction := caHide;
end;

procedure TFL3DReplayForm.FormCreate(Sender: TObject);
var
  TokenHandle: HANDLE;
  ID: LUID;
  Privileges: TTokenPrivileges;
begin
  cbAutoSave.Checked := Options.ReplayAutoSaveName <> '';
  ClientHeight := HEIGHT_HIDE_MEMDUMP;

  // We need debug privileges.
  OpenProcessToken(GetCurrentProcess, TOKEN_QUERY, TokenHandle{%H-});
  LookupPrivilegeValue(nil, SE_DEBUG_NAME, ID{%H-});
  Privileges.Privileges[0].Luid := ID;
  Privileges.Privileges[0].Attributes := SE_PRIVILEGE_ENABLED;
  AdjustTokenPrivileges(TokenHandle, false, @Privileges, SizeOf(Privileges), nil, nil);
end;

procedure TFL3DReplayForm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (ssShift in Shift) and (fUnlockMemdumpProgress < Length(MEMDUMP_UNLOCK_CODE)) then
    fUnlockMemdumpProgress := 0;
end;

procedure TFL3DReplayForm.FormKeyPress(Sender: TObject; var Key: char);
begin
  if fUnlockMemdumpProgress < Length(MEMDUMP_UNLOCK_CODE) then
    if Key = MEMDUMP_UNLOCK_CODE[fUnlockMemdumpProgress + 1] then
    begin
      Inc(fUnlockMemdumpProgress);
      if fUnlockMemdumpProgress = Length(MEMDUMP_UNLOCK_CODE) then
      begin
        btnMemoryDump.Visible := true;
        btnMemoryInject.Visible := true;
        ebInjectOffset.Visible := true;
        mmData.Visible := true;
        btnMemoryInjectFromText.Visible := true;
        cbAutoInject.Visible := true;
        ClientHeight := cbAutoInject.Top + cbAutoInject.Height + btnSaveReplay.Top;
      end;

      Key := #0;
    end;
end;

procedure TFL3DReplayForm.mmDataChange(Sender: TObject);
begin
  cbAutoInject.Checked := false;
end;

procedure TFL3DReplayForm.mmDataKeyPress(Sender: TObject; var Key: char);
begin
  if (Key > ' ') and not (Key in ['0'..'9', 'a'..'f', 'A'..'F']) then
    Key := #0;
end;

procedure TFL3DReplayForm.StickyTimerTimer(Sender: TObject);
begin
  WriteProcessMemory(fDosboxHandle, fVirtualMemoryBase + StrToIntDef('0x' + ebInjectOffset.Text, 0), @fDataBuffer[0], Length(fDataBuffer), nil);
end;

procedure TFL3DReplayForm.CheckAutosave;
var
  Required, Saved: Byte;
  Buffer: array[0..REPLAY_SIZE-1] of Byte;
  F: TFileStream;

  function MakeAutoSaveFilename: String;
  var
    Title: String[32];
    LevelTitle: String;
    n: Integer;

    function MakeTimestamp: String;
    begin
      Result := FormatDateTime('yyyy"-"mm"-"dd"_"hh"-"nn"-"ss', Now);
    end;
  const
    FORBIDDEN_CHARS = [#1..#31, '<', '>', ':', '"', '/', '\', '|', '?', '*', ' '];
  begin
    Result := Options.ReplayAutoSaveName;

    if Pos('#TITLE#', Result) > 0 then
    begin
      ReadProcessMemory(fDosboxHandle, fVirtualMemoryBase + TITLE_OFFSET, @Title[1], 32, nil);
      LevelTitle := '';
      for n := 1 to 32 do
        if Title[n] = #0 then
          Break
        else if Title[n] in FORBIDDEN_CHARS then
          LevelTitle := LevelTitle + '_'
        else
          LevelTitle := LevelTitle + Title[n];
      Result := StringReplace(Result, '#TITLE#', Trim(LevelTitle), [rfReplaceAll]);
    end;

    Result := StringReplace(Result, '#TIMESTAMP#', MakeTimestamp, [rfReplaceAll]);
  end;
begin
  if fDosboxHandle = 0 then Exit;
  if Options.ReplayAutoSaveName = '' then Exit;
  if not (cbAutoSave.Enabled and cbAutoSave.Checked) then Exit;

  ReadProcessMemory(fDosboxHandle, fVirtualMemoryBase + LEMMINGS_SAVED_OFFSET, @Saved, 1, nil);
  ReadProcessMemory(fDosboxHandle, fVirtualMemoryBase + SAVE_REQUIREMENT_OFFSET, @Required, 1, nil);

  if Saved < Required then
    fAutosaveFired := false
  else if not fAutosaveFired and ReadProcessMemory(fDosboxHandle, fVirtualMemoryBase + REPLAY_OFFSET_FROM_BASE, @Buffer[0], REPLAY_SIZE, nil) then
  begin
    fAutosaveFired := true;
    F := TFileStream.Create(MakeAutoSaveFilename, fmCreate);
    try
      F.Write(Buffer[0], REPLAY_SIZE);
    finally
      F.Free;
    end;
  end;
end;

procedure TFL3DReplayForm.ReleaseHandle;
begin
  if fDosboxHandle = 0 then Exit;
  CloseHandle(fDosboxHandle);
  fDosboxHandle := 0;
end;

procedure TFL3DReplayForm.PerformInitialization(Silent: Boolean = true);
var
  DosboxBasePointer: Pointer;

  function FindBasePointer: Boolean;
  var
    Buffer: array[0..511] of Char; // we'll just assume the path to DosBox will never be longer than this.
    ActualLength: Integer;

    Modules: array[0..511] of HModule;
    ModArrayBytes: DWORD;

    DosboxModule: HModule;

    i: Cardinal;
  begin
    Result := false;
    ModArrayBytes := 0;

    if not EnumProcessModules(fDosboxHandle, @Modules[0], SizeOf(Modules), ModArrayBytes) then
    begin
      if not Silent then
        ShowMessage('Error enumerating DOSBox modules.');
      Exit;
    end;

    i := 0;
    DosboxModule := 0;
    while (i < (ModArrayBytes div 4)) and (i < 512) do
    begin
      ActualLength := GetModuleFileNameExA(fDosboxHandle, Modules[i], @Buffer[0], SizeOf(Buffer));
      if Uppercase(RightStr(LeftStr(Buffer, ActualLength), 10)) = 'DOSBOX.EXE' then
      begin
        DosboxModule := Modules[i];
        Break;
      end;

      Inc(i);
    end;

    DosboxBasePointer := Pointer(DosboxModule);
    Result := DosboxBasePointer <> nil;
  end;

  function FindVirtualMemoryBase: Boolean;
  var
    MemPtr: Pointer;
    MemInfo: MEMORY_BASIC_INFORMATION;
    SysInfo: SYSTEM_INFO;
    Buffer: array of Byte;

    i: Integer;
    BytePtr: ^Byte;
    SigCheck: Cardinal;
  const
    KEY_STRING = 'V1.13 26/07/95 17:23:11';
    KEY_STRING_OFFSET = $0001090F;
  type
    TKeyString = array[0..Length(KEY_STRING)-1] of AnsiChar;
  var
    KeyStringPtr: ^TKeyString absolute BytePtr;
  begin
    Result := false;

    if Options.LastKnownBase <> nil then
    begin
      SetLength(Buffer, Length(KEY_STRING));
      if ReadProcessMemory(fDosboxHandle, Options.LastKnownBase + KEY_STRING_OFFSET, @Buffer[0], Length(Buffer), nil) then
      begin
        BytePtr := @Buffer[0];
        if KeyStringPtr^ = KEY_STRING then
        begin
          fVirtualMemoryBase := Options.LastKnownBase;

          SigCheck := 0;
          if ReadProcessMemory(fDosboxHandle, fVirtualMemoryBase + SIGNATURE_OFFSET, @SigCheck, 4, nil) and
             (SigCheck <> SIGNATURE_TERMINATED_VALUE) then
          begin
            Options.LastKnownBase := fVirtualMemoryBase;
            Result := true;
          end else
            fVirtualMemoryBase := nil;

          Exit;
        end;
      end;
    end;

    SetLength(Buffer, 0);

    GetSystemInfo(SysInfo{%H-});
    MemPtr := Pointer(Max(Cardinal(DosboxBasePointer), Cardinal(SysInfo.lpMinimumApplicationAddress)));

    while MemPtr < SysInfo.lpMaximumApplicationAddress do
    begin
      if VirtualQueryEx(fDosboxHandle, MemPtr, @MemInfo, SizeOf(MemInfo)) = 0 then
      begin
        if not Silent then
          ShowMessage('Error querying DOSBox memory pages.');
        Exit;
      end;

      try
        if (MemInfo.State and (MEM_FREE or MEM_RESERVE)) <> 0 then Continue;

        SetLength(Buffer, MemInfo.RegionSize);

        if not ReadProcessMemory(fDosboxHandle, MemInfo.BaseAddress, @Buffer[0], Length(Buffer), nil) then Continue;

        BytePtr := @Buffer[0];
        for i := 0 to Length(Buffer) - Length(KEY_STRING) do
        begin
          if KeyStringPtr^ = KEY_STRING then
          begin
            fVirtualMemoryBase := MemInfo.BaseAddress + i - KEY_STRING_OFFSET;

            SigCheck := 0;
            if ReadProcessMemory(fDosboxHandle, fVirtualMemoryBase + SIGNATURE_OFFSET, @SigCheck, 4, nil) and
               (SigCheck <> SIGNATURE_TERMINATED_VALUE) then
            begin
              Options.LastKnownBase := fVirtualMemoryBase;
              Result := true;
            end else
              fVirtualMemoryBase := nil;

            Exit;
          end;
          Inc(BytePtr);
        end;
      finally
        MemPtr := MemInfo.BaseAddress + MemInfo.RegionSize;
      end;
    end;
  end;

begin
  // First, locate DOSBox process.
  fDosboxPID := FindProcess('DOSBox.exe');
  if fDosboxPID = $FFFFFFFF then
  begin
    if not Silent then
      ShowMessage('Could not find DOSBox. Please ensure DOSBox is running. If DOSBox is running as administrator, L3DReplay must also run as administrator.');
    Exit;
  end;

  // Next, get access to DOSBox process.
  fDosboxHandle := OpenProcess(PROCESS_ALL_ACCESS, false, fDosboxPID);
  if fDosboxHandle = 0 then
  begin
    if not Silent then
      ShowMessage('Could not hook into DOSBox. If DOSBox is running as administrator, L3DReplay must also run as administrator.');
    Exit;
  end;

  // Get info on DOSBox's memory.
  if not FindBasePointer then
  begin
    if not Silent then
      ShowMessage('Couldn''t find DOSBox base memory pointer!');
    ReleaseHandle;
    Exit;
  end;

  // Finally, we need to locate the part of DOSBox's memory where that replay data is.
  if not FindVirtualMemoryBase then
  begin
    if not Silent then
      ShowMessage('L3DReplay was not able to locate the replay data in DOSBox memory. ' +
                  'Please ensure you start playing the demo before selecting the file. ' +
                  'If problems persist, please visit Lemmings Forums to ask for help.');
    ReleaseHandle;
    Exit;
  end;

  btnSaveReplay.Enabled := true;
  btnLoadReplay.Enabled := true;
  btnMemoryDump.Enabled := true;
  btnMemoryInject.Enabled := true;
  btnMemoryInjectFromText.Enabled := true;
  cbAutoSave.Enabled := true;
  cbAutoInject.Enabled := true;
end;

function TFL3DReplayForm.PerformValidityCheck(Silent: Boolean): Boolean;
var
  SigCheck: Cardinal;

  procedure Deinitialize(aNoDosbox: Boolean);
  begin
    btnSaveReplay.Enabled := false;
    btnLoadReplay.Enabled := false;
    if aNoDosbox then
    begin
      btnMemoryDump.Enabled := false;
      btnMemoryInject.Enabled := false;
      btnMemoryInjectFromText.Enabled := false;
      cbAutoInject.Enabled := false;
      cbAutoInject.Checked := false;
    end;
    cbAutoSave.Enabled := false;
    ReleaseHandle;
  end;

begin
  Result := false;
  if fDosboxHandle = 0 then Exit;

  if WaitForSingleObject(fDosboxHandle, 0) = WAIT_OBJECT_0 then
  begin
    Deinitialize(true);
    if not Silent then
      ShowMessage('DOSBox is no longer running!');
    Exit;
  end;

  SigCheck := 0;
  if (not ReadProcessMemory(fDosboxHandle, fVirtualMemoryBase + SIGNATURE_OFFSET, @SigCheck, 4, nil)) or
     (SigCheck = SIGNATURE_TERMINATED_VALUE) then
  begin
    Deinitialize(false);
    if not Silent then
      ShowMessage('Lemmings 3D is no longer running!');
    Exit;
  end;

  Result := true;
end;

function TFL3DReplayForm.MakeDataBuffer: Boolean;
var
  i, n: Integer;
  CurIndex: Integer;
begin
  Result := false;
  SetLength(fDataBuffer, 100);

  CurIndex := 0;
  for i := 0 to mmData.Lines.Count-1 do
  begin
    n := 1;
    while n <= Length(mmData.Lines[i]) do
    begin
      if mmData.Lines[i][n] = ' ' then
      begin
        Inc(n);
        Continue;
      end;

      if (n = Length(mmData.Lines[i])) or
         (not ((mmData.Lines[i][n] in ['0'..'9', 'A'..'F', ' ']) and (mmData.Lines[i][n+1] in ['0'..'9', 'A'..'F', ' ']))) then
      begin
        ShowMessage('Invalid input data.');
        Exit;
      end;

      if CurIndex = Length(fDataBuffer) then
        SetLength(fDataBuffer, CurIndex * 2);

      fDataBuffer[CurIndex] := StrToInt('0x' + mmData.Lines[i][n] + mmData.Lines[i][n+1]);
      Inc(CurIndex);
      Inc(n, 2);
    end;
  end;

  SetLength(fDataBuffer, CurIndex);

  Result := true;
end;

end.

